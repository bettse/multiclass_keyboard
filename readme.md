# multiClass_keyboard


HID multiClass reader output as a keyboard using a teensy


![video demonstration](demo.mp4)


## LEDs

LEDs can be controlled by setting caps lock light for green LED or num lock light for red LED.  On Macos, [setledsmac](https://github.com/damieng/setledsmac) works well, but seems to require running with `sudo`.
