#include <Wiegand.h>

// These are the pins connected to the Wiegand D0 and D1 signals.
// Ensure your board supports external Interruptions on these pins
#define PIN_D0 4
#define PIN_D1 21

#define PIN_GREEN_LED 8
#define PIN_RED_LED 17

// The object that handles the wiegand protocol
Wiegand wiegand;

// When any of the pins have changed, update the state of the wiegand library
void pinStateChanged() {
  wiegand.setPin0State(digitalRead(PIN_D0));
  wiegand.setPin1State(digitalRead(PIN_D1));
}

// Notifies when a reader has been connected or disconnected.
// Instead of a message, the seconds parameter can be anything you want -- Whatever you specify on `wiegand.onStateChange()`
void stateChanged(bool plugged, const char *message) {
  Serial.print(message);
  Serial.println(plugged ? "CONNECTED" : "DISCONNECTED");
}

// Notifies when a card was read.
// Instead of a message, the third parameter can be anything you want -- Whatever you specify on `wiegand.onReceive()`
void receivedData(uint8_t *data, uint8_t bits, const char *message) {
  Serial.print(message);
  Serial.print(bits);
  Serial.print("bits / ");
  //Print value in HEX
  uint8_t bytes = (bits + 7) / 8;
  for (int i = 0; i < bytes; i++) {
    Serial.printf("%02x", data[i]);
  }
  Serial.println();

  digitalWrite(PIN_GREEN_LED, LOW); // High = Off
  if (bits == 4) {
    switch (data[0]) {
      case 10: // keypad *
        Keyboard.print('*');
        break;
      case 11: // keypad #
        Keyboard.print('#');
        break;
      default:
        Keyboard.print(data[0]);
        break;
    }
  } else if (bits == 24) {
    Keyboard.print(data[0]);// FC
    Keyboard.print(":");
    uint16_t cn = (data[1] << 8) | data[2];
    Keyboard.print(cn);
  }
  digitalWrite(PIN_GREEN_LED, HIGH); // High = Off
}

// Notifies when an invalid transmission is detected
void receivedDataError(Wiegand::DataError error, uint8_t *rawData, uint8_t rawBits, const char *message) {
  Serial.print(message);
  Serial.print(Wiegand::DataErrorStr(error));
  Serial.print(" - Raw data: ");
  Serial.print(rawBits);
  Serial.print("bits / ");

  //Print value in HEX
  uint8_t bytes = (rawBits + 7) / 8;
  for (int i = 0; i < bytes; i++) {
    Serial.print(rawData[i] >> 4, 16);
    Serial.print(rawData[i] & 0xF, 16);
  }
  Serial.println();

  delay(200);
  digitalWrite(PIN_RED_LED, HIGH); // High = Off
}

// Initialize Wiegand reader
void setup() {
  Serial.begin(115200);

  //Install listeners and initialize Wiegand reader
  wiegand.onReceive(receivedData, "Card read: ");
  wiegand.onReceiveError(receivedDataError, "Card read error: ");
  wiegand.onStateChange(stateChanged, "State changed: ");
  wiegand.begin(Wiegand::LENGTH_ANY, true);

  //initialize pins as INPUT and attaches interruptions
  pinMode(PIN_D0, INPUT);
  pinMode(PIN_D1, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_D0), pinStateChanged, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_D1), pinStateChanged, CHANGE);

  // LEDs
  pinMode(PIN_RED_LED, OUTPUT);
  pinMode(PIN_GREEN_LED, OUTPUT);
  digitalWrite(PIN_GREEN_LED, HIGH); // High = Off
  digitalWrite(PIN_RED_LED, HIGH); // High = Off

  //Sends the initial pin state to the Wiegand library
  pinStateChanged();
}

// Every few milliseconds, check for pending messages on the wiegand reader
// This executes with interruptions disabled, since the Wiegand library is not thread-safe
void loop() {
  noInterrupts();
  wiegand.flush();
  interrupts();
  digitalWrite(PIN_GREEN_LED, isCapsOn() ? LOW : HIGH); // High = Off
  digitalWrite(PIN_RED_LED, isNumOn() ? LOW : HIGH); // High = Off
}

bool isNumOn(){
  return (keyboard_leds & 1) == 1;
}

bool isCapsOn(){
  return (keyboard_leds & 2) == 2;
}

bool isScrollOn(){
  return (keyboard_leds & 4) == 4;
}
